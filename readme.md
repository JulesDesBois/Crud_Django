install virtualenv
```
virtualenv -p python3 ~/venv3
```

activate venv3
```
source venv/bin/activate
```

install Django
```
pip install django
```

install Selenium
```
pip install selenium
```

launch serveur
```
./manage.py runserver
```

consult listing:
		http://localhost:8000/lesTaches/listing
