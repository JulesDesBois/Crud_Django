from selenium import webdriver
import unittest

class UnitTest1(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(executable_path='./usr/lib/chromium-browser//chromedriver')
        self.browser.implicitly_wait(3)
    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Alice se rend sur le site localhost:8000
        # et espère y trouver une app de Planning
        self.browser.get('http://localhost:8000/lesTaches/listing')

        # Elle remaruqe dans le titre de la page le mot "Planning"
        self.assertIn('Liste', self.browser.title)  #
        self.fail('Test terminé !')  #

        # Elle peut aussi ajouter une tâche à réaliser ...
        # Puis la consulter ....

if __name__ == '__main__':
    unittest.main(warnings='ignore')
