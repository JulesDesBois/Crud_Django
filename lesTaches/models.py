from django.db import models

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    closed = models.BooleanField(default=False)
    schedule_date = models.DateField(auto_now_add=True)
    due_date = models.DateField(auto_now_add=False)

    def __str__(self):
        return 'Nom de la tâche :' + self.name + 'Description : ' + self.description


    def colored_due_date(self):
        due_date=django_date(self.due_date,"d F Y")
        return format_html("<span style=color:%s>%s</span>"%(color_due_date))

    colored_due_date.allows_tags= True
