from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.forms import ModelForm
from django.urls import reverse
from django import forms
from .models import Task

# Create your views here.
def home(request, name=None):
	return HttpResponse('bonjour ' + name if name is not None else '')

def taskListing(request):
	from django.template import Template, Context
	objets = Task.objects.all().order_by('due_date')
	#template=Template('{%for elem in objets %} {{elem}} <br/> {%endfor%}')
	#print(str(template))
	#context=Context({'objets':objets})
	#print(str(template.render(context)))
	#return HttpResponse(template.render(context))
	return render(request,'lesTaches/listing.html',{'objets':objets})

class formTask(ModelForm):
    class Meta:
        model=Task
        widgets = {
          'description': forms.Textarea(attrs={'rows':4, 'cols':30}),
        }
        fields=("name","description","due_date")

def addTask(request):
    if request.method == 'POST':
        form = formTask(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('listing'))
    else:
        formAddTask = formTask()
        return render(request,'lesTaches/addTask.html',{'form' : formAddTask})

def editTask(request,name):
    formEditTask = Task.objects.get(name = name)
    if request.method == 'POST':
        formEditTask = formTask(request.POST,instance=formEditTask)
        if formEditTask.is_valid():
            formEditTask.save()
            return HttpResponseRedirect(reverse('listing'))
    else:
        formEditTask = formTask(instance=formEditTask)
        return render(request,'lesTaches/editTask.html',{"form":formEditTask})

def deleteTask(request,name):
    task = Task.objects.filter(name = name)
    task.delete()
    return HttpResponseRedirect(reverse('listing'))
